<?php

namespace Drupal\search_api_sajari\Plugin\search_api\backend;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Query\QueryInterface;
use GuzzleHttp\Client;
use Sajari\ApiException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sajari\Configuration;
use Sajari\Model\Collection;
use Sajari\Model\QueryCollectionRequest;
use Sajari\Model\UpsertRecordRequest;
use Sajari\Model\DeleteRecordRequest;
use Sajari\Model\SchemaField;
use Sajari\Model\RecordKey;
use Sajari\Model\GeneratePipelinesRequest;
use Sajari\Model\SetDefaultVersionRequest;
use Sajari\Model\PipelineStep;
use Sajari\Model\PipelineStepParamBinding;
use Sajari\Api\SchemaApi;
use Sajari\Api\RecordsApi;
use Sajari\Api\CollectionsApi;
use Sajari\Api\PipelinesApi;

/**
 * Indexes items using the Search.io API.
 *
 * @SearchApiBackend(
 *   id = "sajari",
 *   label = @Translation("Search.io"),
 *   description = @Translation("Indexes items using the Search.io API.")
 * )
 */
class SearchApiSajariBackend extends BackendPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * The database connection used by this plugin.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Schema API instance.
   *
   * @var \Sajari\Api\SchemaApi
   */
  protected $schemaApi;

  /**
   * Records API instance.
   *
   * @var \Sajari\Api\RecordsApi
   */
  protected $recordsApi;

  /**
   * Collection API instance.
   *
   * @var \Sajari\Api\CollectionsApi
   */
  protected $collectionApi;

  /**
   * Pipelines API instance.
   *
   * @var \Sajari\Api\PipelinesApi
   */
  protected $pipelinesApi;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $plugin->connection = $container->get('database');
    $plugin->setEntityTypeManager($container->get('entity_type.manager'));

    return $plugin;
  }

  /**
   * Retrieves the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager() {
    return $this->entityTypeManager ?: \Drupal::entityTypeManager();
  }

  /**
   * Sets the entity type manager service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @return $this
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    return $this;
  }

  /**
   * Gets the Search.io Collection API client.
   */
  protected function getCollectionApi() {
    if (!isset($this->collectionApi)) {
      try {
        // Configure HTTP basic authorization: BasicAuth.
        $config = Configuration::getDefaultConfiguration()
          ->setUsername($this->configuration['key'])
          ->setPassword($this->configuration['secret']);

        $this->collectionApi = new CollectionsApi(new Client(), $config);
      }
      catch (\Exception $e) {
        $message = t('There was a problem connecting to the Collection API please check your credentials: @message', [
          '@message' => $e->getMessage(),
        ]);

        throw new \Exception($message);
      }
    }

    return $this->collectionApi;
  }

  /**
   * Gets the Search.io Schema API client.
   */
  protected function getSchemaApi() {
    if (!isset($this->schemaApi)) {
      try {
        // Configure HTTP basic authorization: BasicAuth.
        $config = Configuration::getDefaultConfiguration()
          ->setUsername($this->configuration['key'])
          ->setPassword($this->configuration['secret']);

        $this->schemaApi = new SchemaApi(new Client(), $config);
      }
      catch (\Exception $e) {
        $message = t('There was a problem connecting to the Schema API please check your credentials: @message', [
          '@message' => $e->getMessage(),
        ]);

        throw new \Exception($message);
      }
    }

    return $this->schemaApi;
  }

  /**
   * Gets the Search.io Records API client.
   */
  protected function getRecordsApi() {
    if (!isset($this->recordsApi)) {
      try {
        // Configure HTTP basic authorization: BasicAuth.
        $config = Configuration::getDefaultConfiguration()
          ->setUsername($this->configuration['key'])
          ->setPassword($this->configuration['secret']);

        $this->recordsApi = new RecordsApi(new Client(), $config);
      }
      catch (\Exception $e) {
        $message = t('There was a problem connecting to the Records API please check your credentials: @message', [
          '@message' => $e->getMessage(),
        ]);

        throw new \Exception($message);
      }
    }

    return $this->recordsApi;
  }

  /**
   * Gets the Search.io Pipelines API client.
   */
  protected function getPipelinesApi() {
    if (!isset($this->pipelinesApi)) {
      try {
        // Configure HTTP basic authorization: BasicAuth.
        $config = Configuration::getDefaultConfiguration()
          ->setUsername($this->configuration['key'])
          ->setPassword($this->configuration['secret']);

        $this->pipelinesApi = new PipelinesApi(new Client(), $config);
      }
      catch (\Exception $e) {
        $message = t('There was a problem connecting to the Pipelines API please check your credentials: @message', [
          '@message' => $e->getMessage(),
        ]);

        throw new \Exception($message);
      }
    }

    return $this->pipelinesApi;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'base_url' => '',
      'key' => '',
      'secret' => '',
      'collection_id' => '',
      'sitename_value' => '',
      'sitename_field' => '',
      'fields' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Prepare description links.
    $sajari_link = Link::fromTextAndUrl('Search.io', Url::fromUri('https://www.search.io/console/'))
      ->toString();

    $form['base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URL'),
      '#description' => $this->t('The base url to use in the content paths to make them absolute.'),
      '#default_value' => $this->configuration['base_url'],
      '#required' => TRUE,
    ];

    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key ID'),
      '#description' => $this->t('The @sajari "API key" from "Account" or "Collection" credentials section).', [
        '@sajari' => $sajari_link,
      ]),
      '#default_value' => $this->configuration['key'],
      '#required' => TRUE,
    ];

    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret'),
      '#description' => $this->t('The @sajari "API secret" from "Account" or "Collection" credentials section.', [
        '@sajari' => $sajari_link,
      ]),
      '#default_value' => $this->configuration['secret'],
      '#required' => TRUE,
    ];

    $form['collection_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Collection ID'),
      '#description' => $this->t('The @sajari "Collection ID" (a new collection will be created if "Account" credentials were provided).', [
        '@sajari' => $sajari_link,
      ]),
      '#default_value' => $this->configuration['collection_id'],
      '#required' => TRUE,
    ];

    $form['sitename_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sitename Search.io Field'),
      '#description' => $this->t('The name of the field to use on Search.io to sync the sitename. Must begin with a letter and contain only letters, numbers or _'),
      '#default_value' => $this->configuration['sitename_field'],
      '#required' => TRUE,
    ];

    $form['sitename_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sitename'),
      '#default_value' => $this->configuration['sitename_value'],
      '#description' => $this->t('Human readable sitename to use in tabs or facets.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    try {
      // Check if the key and secret are good to use.
      $collectionApi = $this->getCollectionApi();
      $collectionApi->listCollections();
    }
    catch (\Exception $e) {
      $message = $this->t('Wrong key or secret: @message', [
        '@message' => $e->getMessage(),
      ]);
      $form_state->setErrorByName('key', $message);
      $form_state->setErrorByName('secret', $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    try {
      $this->configuration['base_url'] = $form_state->getValue('base_url');
      $this->configuration['key'] = $form_state->getValue('key');
      $this->configuration['secret'] = $form_state->getValue('secret');
      $this->configuration['collection_id'] = $form_state->getValue('collection_id');
      $this->configuration['sitename_value'] = $form_state->getValue('sitename_value');
      $this->configuration['sitename_field'] = $form_state->getValue('sitename_field');

      try {
        $collectionApi = $this->getCollectionApi();
      }
      catch (\Exception $e) {
        $message = $this->t('Exception while connecting with the Collection API.');
        $this->getLogger()->error($e->getMessage());
        $this->getMessenger()->addError($message);
        return;
      }

      try {
        // Check if the specified collection exists.
        $collectionApi->getCollection($this->configuration['collection_id']);
      }
      catch (ApiException $e) {
        // Create a new collection with the specified ID.
        $collection = new Collection([
          'display_name' => $this->configuration['collection_id'],
        ]);
        $collectionApi->createCollection($this->configuration['collection_id'], $collection);
      }
    }
    catch (\Exception $e) {
      $message = $this->t('Exception when calling createCollection() @message', [
        '@message' => $e->getMessage(),
      ]);

      $this->getMessenger()->addError($message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings() {
    $info[] = [
      'label' => $this->t('Search.io API'),
      'info' => Json::encode($this->configuration),
    ];
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function addIndex(IndexInterface $index) {
    $this->updateIndex($index);
  }

  /**
   * {@inheritdoc}
   */
  public function updateIndex(IndexInterface $index) {
    $this->updateSchema($index);
  }

  /**
   * Updates the Search.io schema for specific index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The updated index.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   If the Search.io API error occurs.
   */
  protected function updateSchema(IndexInterface $index) {
    try {
      $schemaApi = $this->getSchemaApi();
    }
    catch (\Exception $e) {
      $message = $this->t('Exception while connecting with the Schema API.');

      $this->getLogger()->error($e->getMessage());
      $this->getMessenger()->addError($message);
      return;
    }

    $new_fields = [];
    $indexed_fields = [];
    $autocomplete_fields = [];
    $existing_fields = [];
    $array_fields = [];

    // Define default data type overrides.
    $type_mapping = [
      'text' => 'STRING',
      'string' => 'STRING',
      'integer' => 'INTEGER',
      'decimal' => 'DOUBLE',
      'date' => 'TIMESTAMP',
      'boolean' => 'BOOLEAN',
    ];

    // Gets all the existing fields.
    try {
      $schema_fields = $schemaApi->listSchemaFields($this->configuration['collection_id'])
        ->getSchemaFields();
    }
    catch (\Exception $e) {
      $message = $this->t('Exception when calling listSchemaFields()');

      $this->getLogger()->error($e->getMessage());
      $this->getMessenger()->addError($message);
      return;
    }

    foreach ($schema_fields as $schema_field) {
      $existing_fields[$schema_field->getName()] = $schema_field->getType();
    }

    // Add sitename field to schema.
    if (!empty($this->configuration['sitename_field'])) {
      if (!isset($existing_fields[$this->configuration['sitename_field']])) {
        try {
          // Send API request to create a new sitename field.
          $schema_field = new SchemaField([
            'name' => $this->configuration['sitename_field'],
            'description' => 'Sitename Identifier',
            'type' => 'STRING',
            'mode' => 'NULLABLE',
            'array' => FALSE,
          ]);

          $schemaApi->createSchemaField($this->configuration['collection_id'], $schema_field);
          $indexed_fields[] = $this->configuration['sitename_field'];
          $new_fields[] = $this->configuration['sitename_field'];
          $existing_fields[$this->configuration['sitename_field']] = 'STRING';
        }
        catch (\Exception $e) {
          $message = $this->t('Exception when calling createSchemaField()');

          $this->getLogger()->error($e->getMessage());
          $this->getMessenger()->addError($message);
          return;
        }
      }
    }

    // Add UUID field to schema.
    if (!isset($existing_fields['uuid'])) {
      try {
        // Send API request to create a new UUID field.
        $schema_field = new SchemaField([
          'name' => 'uuid',
          'description' => 'Drupal UUID (Universally Unique Identifier)',
          'type' => 'STRING',
          'mode' => 'UNIQUE',
          'array' => FALSE,
        ]);

        $schemaApi->createSchemaField($this->configuration['collection_id'], $schema_field);
        $indexed_fields[] = 'uuid';
        $new_fields[] = 'uuid';
        $existing_fields['uuid'] = 'STRING';
      }
      catch (\Exception $e) {
        $message = $this->t('Exception when calling createSchemaField()');

        $this->getLogger()->error($e->getMessage());
        $this->getMessenger()->addError($message);
        return;
      }
    }

    // Add index_item_id field to schema.
    if (!isset($existing_fields['index_item_id'])) {
      try {
        // Send API request to create a new index_item_id field.
        $schema_field = new SchemaField([
          'name' => 'index_item_id',
          'description' => 'Search API Index Item ID',
          'type' => 'STRING',
          'mode' => 'UNIQUE',
          'array' => FALSE,
        ]);

        $schemaApi->createSchemaField($this->configuration['collection_id'], $schema_field);
        $indexed_fields[] = 'index_item_id';
        $new_fields[] = 'index_item_id';
        $existing_fields['index_item_id'] = 'STRING';
      }
      catch (\Exception $e) {
        $message = $this->t('Exception when calling createSchemaField()');

        $this->getLogger()->error($e->getMessage());
        $this->getMessenger()->addError($message);
        return;
      }
    }

    // Process all configured fields.
    foreach ($index->getFields() as $field_id => $field) {
      $property_type = $field->getType();
      $field_options = $this->configuration['fields'][$field_id];
      $field_options = json_decode($field_options, TRUE);

      // Override data type if needed.
      if (isset($type_mapping[$property_type])) {
        $property_type = $type_mapping[$property_type];
      }
      else {
        $property_type = 'STRING';
      }

      // Only use string fields for the pipeline creation.
      if ($property_type == 'STRING' && !$field_options['list']) {
        // List of fields to use in queries.
        if ($field_options['indexed']) {
          $indexed_fields[] = $field_id;
        }
      }

      // List of fields to train the autocomplete model.
      if ($field_options['autocomplete']) {
        $autocomplete_fields[] = $field_id;
      }

      if (!isset($existing_fields[$field_id])) {
        $new_fields[] = $field_id;

        if (!empty($field_options)) {
          try {
            if ($field_options['list']) {
              $array_fields[$field_id] = $property_type;
            }

            // Send API request to create a new schema field.
            $schema_field = new SchemaField([
              'name' => $field->getFieldIdentifier(),
              'description' => $field->getLabel(),
              'type' => $property_type,
              'mode' => $field_options['mode'],
              'array' => $field_options['list'],
            ]);

            $schemaApi->createSchemaField($this->configuration['collection_id'], $schema_field);
          }
          catch (\Exception $e) {
            $message = $this->t('Exception when calling createSchemaField()');

            $this->getLogger()->error($e->getMessage());
            $this->getMessenger()->addError($message);
            return;
          }
        }
      }
    }

    try {
      $pipelinesApi = $this->getPipelinesApi();
    }
    catch (\Exception $e) {
      $message = $this->t('Exception while connecting with the Pipelines API.');

      $this->getLogger()->error($e->getMessage());
      $this->getMessenger()->addError($message);
      return;
    }

    try {
      $existing_pipelines = $pipelinesApi->listPipelines(
        $this->configuration['collection_id'])->getPipelines();
    }
    catch (Exception $e) {
      $message = $this->t('Exception when calling PipelinesApi->listPipelines()');

      $this->getLogger()->error($e->getMessage());
      $this->getMessenger()->addError($message);
      return;
    }

    // If we don't have any new fields then we don't need to create pipelines.
    if (empty($new_fields) && count($existing_pipelines) >= 3) {
      return;
    }

    // Generate pipelines templates.
    try {
      $generate_pipelines_request = new GeneratePipelinesRequest([
        'searchable_fields' => $indexed_fields,
        'query_training_fields' => $autocomplete_fields,
      ]);
      $pipelines_templates = $pipelinesApi->generatePipelines($this->configuration['collection_id'], $generate_pipelines_request);
      $version = time();
    }
    catch (\Exception $e) {
      $message = $this->t('Exception when calling generatePipelines()');

      $this->getLogger()->error($e->getMessage());
      $this->getMessenger()->addError($message);
      return;
    }

    // Record Pipeline.
    try {
      if (count($existing_pipelines) < 3) {
        $record_pipeline = $pipelines_templates->getRecordPipeline();
      }
      else {
        $record_pipeline = $this->getDefaultPipeline('RECORD', 'app', $new_fields, $autocomplete_fields, $array_fields);
      }
      $record_pipeline->setName('app');
      $record_pipeline->setVersion('v' . $version);
      $pipelinesApi->createPipeline($this->configuration['collection_id'], $record_pipeline);

      if (count($existing_pipelines) >= 3) {
        $set_default_version_request = new SetDefaultVersionRequest([
          'version' => 'v' . $version,
        ]);

        $pipelinesApi->setDefaultVersion(
          $this->configuration['collection_id'],
          'RECORD',
          'app',
          $set_default_version_request
        );
      }
    }
    catch (\Exception $e) {
      $message = $this->t('Exception during Record pipeline creation: @message', [
        '@message' => $e->getMessage(),
      ]);

      $this->getLogger()->error($message);
    }

    $new_indexed_fields = array_intersect($indexed_fields, $new_fields);
    $new_autocomplete_fields = array_intersect($autocomplete_fields, $new_fields);

    // If we don't have new indexed fields then we don't create QUERY pipelines.
    if (!empty($new_indexed_fields) || count($existing_pipelines) < 3) {
      try {
        if (count($existing_pipelines) < 3) {
          $query_pipeline = $pipelines_templates->getQueryPipeline();
        }
        else {
          $query_pipeline = $this->getDefaultPipeline('QUERY', 'app', $indexed_fields, $autocomplete_fields);
        }
        $query_pipeline->setName('app');
        $query_pipeline->setVersion('v' . $version);
        $pipelinesApi->createPipeline($this->configuration['collection_id'], $query_pipeline);

        if (count($existing_pipelines) >= 3) {
          $set_default_version_request = new SetDefaultVersionRequest([
            'version' => 'v' . $version,
          ]);

          $pipelinesApi->setDefaultVersion(
            $this->configuration['collection_id'],
            'QUERY',
            'app',
            $set_default_version_request
          );
        }
      }
      catch (\Exception $e) {
        $message = $this->t('Exception during Query pipeline creation: @message', [
          '@message' => $e->getMessage(),
        ]);

        $this->getLogger()->error($message);
      }
    }

    // If we don't have new fields then we don't create AUTOCOMPLETE pipelines.
    if (!empty($new_autocomplete_fields) || count($existing_pipelines) < 3) {
      try {
        if (count($existing_pipelines) < 3) {
          $autocomplete_pipeline = $pipelines_templates->getAutocompletePipeline();
        }
        else {
          $autocomplete_pipeline = $this->getDefaultPipeline('QUERY', 'autocomplete', $autocomplete_fields, $autocomplete_fields);
        }
        $autocomplete_pipeline->setName('autocomplete');
        $autocomplete_pipeline->setVersion('v' . $version);
        $pipelinesApi->createPipeline($this->configuration['collection_id'], $autocomplete_pipeline);

        if (count($existing_pipelines) >= 3) {
          $set_default_version_request = new SetDefaultVersionRequest([
            'version' => 'v' . $version,
          ]);

          $pipelinesApi->setDefaultVersion(
            $this->configuration['collection_id'],
            'QUERY',
            'autocomplete',
            $set_default_version_request
          );
        }
      }
      catch (\Exception $e) {
        $message = $this->t('Exception during Autocomplete pipeline creation: @message', [
          '@message' => $e->getMessage(),
        ]);

        $this->getLogger()->error($message);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function indexItems(IndexInterface $index, array $items) {
    $indexed = [];

    foreach ($items as $id => $item) {
      try {
        $this->indexItem($item);
        $indexed[] = $id;
      }
      catch (\Exception $e) {
        $this->getLogger()->error($e->getMessage());
      }
    }

    return $indexed;
  }

  /**
   * Indexes a single item on the specified index.
   *
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The item to index.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   If the Search.io API error occurs.
   */
  protected function indexItem(ItemInterface $item) {
    $item_values = [];

    try {
      $recordsApi = $this->getRecordsApi();
    }
    catch (\Exception $e) {
      $message = $this->t('Exception while connecting with the Records API.');

      $this->getLogger()->error($e->getMessage());
      $this->getMessenger()->addError($message);
      return;
    }

    // Process all configured fields.
    foreach ($item->getFields() as $field_id => $field) {
      $value = $field->getValues();

      if (is_array($value)) {
        if (count($value) == 1) {
          $value = reset($value);
        }
      }

      // Make URL absolute for URI field.
      if ($field->getPropertyPath() === 'search_api_url') {
        if (strpos($value, $this->configuration['base_url']) === FALSE) {
          $value = $this->configuration['base_url'] . $value;
        }
      }

      if (!empty($value)) {
        $item_values[$field_id] = $value;
      }
    }

    // Add sitename field with value.
    if (!empty($this->configuration['sitename_field']) && !empty($this->configuration['sitename_value'])) {
      $item_values[$this->configuration['sitename_field']] = $this->configuration['sitename_value'];
    }

    // Add UUID field with value.
    try {
      $original_object = $item->getOriginalObject();
      $language = $item->getLanguage();
      $item_values['uuid'] = $original_object->get('uuid')->value . $language;
    }
    catch (\Exception $e) {
      $message = $this->t('Exception when getting the uuid of an entity.');

      $this->getLogger()->error($e->getMessage());
      $this->getMessenger()->addError($message);
    }

    // Add Search API Index Item ID field with value.
    try {
      $item_values['index_item_id'] = $item->getId();
      $sitename_machine_name = '_';

      if (!empty($this->configuration['sitename_value'])) {
        $sitename_machine_name = $this->getMachineName($this->configuration['sitename_value']);
      }

      $item_values['index_item_id'] = $sitename_machine_name . $item_values['index_item_id'];
    }
    catch (\Exception $e) {
      $message = $this->t('Exception when getting the ID of an index item.');

      $this->getLogger()->error($e->getMessage());
      $this->getMessenger()->addError($message);
    }

    try {
      // Send values if available.
      if (!empty($item_values)) {
        $upsert_record_request = new UpsertRecordRequest([
          'record' => $item_values,
        ]);
        $recordsApi->upsertRecord($this->configuration['collection_id'], $upsert_record_request);
      }
    }
    catch (\Exception $e) {
      $message = $this->t('Exception when calling upsertRecord()');

      $this->getLogger()->error($e->getMessage());
      $this->getMessenger()->addError($message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItems(IndexInterface $index, array $item_ids) {
    try {
      $recordsApi = $this->getRecordsApi();
    }
    catch (\Exception $e) {
      $message = $this->t('Exception while connecting with the Records API.');

      $this->getLogger()->error($e->getMessage());
      $this->getMessenger()->addError($message);
      return;
    }

    // Get the unique fields from the entities to remove.
    foreach ($item_ids as $item_id) {
      try {
        // Example: entity:node/18:en.
        $item_components = explode(':', $item_id);

        // Eg: node/18.
        $entity_components = explode('/', $item_components[1]);

        if (count($entity_components) != 2) {
          continue;
        }

        // Load the entity.
        $entity = $this->getEntityTypeManager()->getStorage($entity_components[0])->load($entity_components[1]);

        if (!isset($entity)) {
          $sitename_machine_name = '_';

          if (!empty($this->configuration['sitename_value'])) {
            $sitename_machine_name = $this->getMachineName($this->configuration['sitename_value']);
          }

          $key = new RecordKey([
            'field' => 'index_item_id',
            'value' => $sitename_machine_name . $item_id,
          ]);
        }
        else {
          // Get fields to build the unique record key.
          $uuid = $entity->get('uuid')->value;
          $language = $entity->get('langcode')->value;

          if (isset($uuid) && isset($language)) {
            $key = new RecordKey([
              'field' => 'uuid',
              'value' => $uuid . $language,
            ]);
          }
        }
      }
      catch (\Exception $e) {
        $message = $this->t('Exception when getting the uuid of an entity.');

        $this->getLogger()->error($e->getMessage());
        $this->getMessenger()->addError($message);
      }

      try {
        $delete_record_request = new DeleteRecordRequest([
          'key' => $key,
        ]);
        $recordsApi->deleteRecord($this->configuration['collection_id'], $delete_record_request);
      }
      catch (\Exception $e) {
        $message = $this->t('Exception when calling deleteRecord()');

        $this->getLogger()->error($e->getMessage());
        $this->getMessenger()->addError($message);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllIndexItems(IndexInterface $index, $datasource_id = NULL) {
    $result = $this->getIndexedItems($index->id());
    if (!empty($result)) {
      $this->deleteItems($index, $result);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query) {
    try {
      $collectionApi = $this->getCollectionApi();
    }
    catch (\Exception $e) {
      $message = $this->t('Exception while connecting with the Collection API.');

      $this->getLogger()->error($e->getMessage());
      $this->getMessenger()->addError($message);
    }

    $results = $query->getResults();
    $keys = $query->getKeys();

    if (!empty($keys)) {
      try {
        $query_collection_request = new QueryCollectionRequest([
          'variables' => ["q" => $keys],
        ]);

        $query_collection_response = $collectionApi->queryCollection($this->configuration['collection_id'], $query_collection_request);
        $query_collection_results = $query_collection_response->getResults();
        $results->setResultCount($query_collection_response->getTotalSize());

        foreach ($query_collection_results as $key => $query_result) {
          $item = $this->getFieldsHelper()->createItem($query->getIndex(), $key);
          $results->addResultItem($item);
        }

        return $results;
      }
      catch (\Exception $e) {
        $message = $this->t('Exception when calling queryCollection()');

        $this->getLogger()->error($e->getMessage());
        $this->getMessenger()->addError($message);
      }
    }
  }

  /**
   * Gets an array of items_id of a given index_id.
   *
   * @param string $index_id
   *   The search api index ID.
   *
   * @return array
   *   An array of \Drupal\search_api\Item\ItemInterface IDs.
   */
  protected function getIndexedItems($index_id) {
    $query = $this->connection->select('search_api_item', 'sai')
      ->condition('sai.index_id', $index_id)
      ->fields('sai', ['item_id']);

    return $query->execute()->fetchCol();
  }

  /**
   * Get the default version for a given pipeline.
   *
   * @param string $type
   *   The pipeline type: "TYPE_UNSPECIFIED" "RECORD" "QUERY".
   * @param string $name
   *   The name of the pipeline to get the default version of, e.g. my-pipeline.
   * @param array $index_fields
   *   The new "Index" fields added in the index.
   * @param array $autocomplete_fields
   *   The new "Autocomplete" fields added in the index.
   * @param array $array_fields
   *   The new "Array" fields added in the index.
   *
   * @return object
   *   \Sajari\Model\Pipeline.
   */
  protected function getDefaultPipeline($type, $name, array $index_fields, array $autocomplete_fields, array $array_fields = []) {
    try {
      // Combine new index and autocomplete fields to use on pipelines.
      $combined_string_fields = array_unique(array_merge($index_fields, $autocomplete_fields), SORT_REGULAR);

      $pipelinesApi = $this->getPipelinesApi();
      // Get default pipeline.
      $default_pipeline = $pipelinesApi->getDefaultVersion(
        $this->configuration['collection_id'],
        $type,
        $name,
        'FULL'
      );

      // Process and alter pre steps.
      $pre_steps = $default_pipeline->getPreSteps();
      $altered_pre_steps = $pre_steps;

      // Process array fields to create special.
      foreach ($array_fields as $field_id => $field_type) {
        $new_step = new PipelineStep();
        $new_step->setId('convert-field');
        $new_step->setTitle('Coerce single values into an array');
        $new_step->setDescription('If the field contains a single value then convert it into an array.');
        $params = [];

        // Field parameter.
        $field_param = [
          'constant' => $field_id,
        ];

        $params['field'] = new PipelineStepParamBinding($field_param);

        // Repeated parameter.
        $repeated_param = [
          'constant' => "true",
        ];

        $params['repeated'] = new PipelineStepParamBinding($repeated_param);

        $type_name_param = [
          'constant' => $field_type,
        ];

        // Type Name parameter.
        $params['typeName'] = new PipelineStepParamBinding($type_name_param);

        $new_step->setParams($params);
        array_unshift($altered_pre_steps, $new_step);
      }

      foreach ($pre_steps as $key => $step) {
        $params = (array) $step->getParams();

        if (array_key_exists('fields', $params)) {
          $fields = $params['fields'];

          $data = [
            'bind' => $fields->getBind(),
            'description' => $fields->getDescription(),
            'default_value' => $fields->getDefaultValue(),
          ];
          $contants = $fields->getConstant();

          if (isset($contants)) {
            $fields_array = explode(',', $contants);
            $fields_array = array_unique(array_merge($fields_array, $index_fields), SORT_REGULAR);

            if (!empty($fields_array)) {
              $contants = implode(',', $fields_array);
              $data['constant'] = $contants;
              $params['fields'] = new PipelineStepParamBinding($data);
              $altered_pre_steps[$key] = $step->setParams($params);
            }
          }
        }

        // Autocomplete pipeline.
        if (array_key_exists('labelWeights', $params)) {
          $label_weights = $params['labelWeights'];

          $data = [
            'bind' => $label_weights->getBind(),
            'description' => $label_weights->getDescription(),
            'default_value' => $label_weights->getDefaultValue(),
          ];
          $contants = $label_weights->getConstant();

          if (isset($contants) && !empty($autocomplete_fields)) {
            $fields_array = explode(',', $contants);
            $new_fields_combined = implode(',', $autocomplete_fields);
            $new_fields_combined = str_replace(',', ':1.0,', $new_fields_combined) . ':1.0';
            $new_fields_combined = explode(',', $new_fields_combined);
            $fields_array = array_unique(array_merge($fields_array, $new_fields_combined), SORT_REGULAR);

            if (!empty($fields_array)) {
              $contants = implode(',', $fields_array);
              $data['constant'] = $contants;
              $params['labelWeights'] = new PipelineStepParamBinding($data);
              $altered_pre_steps[$key] = $step->setParams($params);
            }
          }
        }

        // Query pipeline.
        if (array_key_exists('phraseLabelWeights', $params)) {
          $phrase_label_weights = $params['phraseLabelWeights'];

          $data = [
            'bind' => $phrase_label_weights->getBind(),
            'description' => $phrase_label_weights->getDescription(),
            'default_value' => $phrase_label_weights->getDefaultValue(),
          ];
          $contants = $phrase_label_weights->getConstant();

          if (isset($contants) && !empty($autocomplete_fields)) {
            $fields_array = explode(',', $contants);
            $new_fields_combined = implode(',', $autocomplete_fields);
            $new_fields_combined = str_replace(',', ':1.0,', $new_fields_combined) . ':1.0';
            $new_fields_combined = explode(',', $new_fields_combined);
            $fields_array = array_unique(array_merge($fields_array, $new_fields_combined), SORT_REGULAR);

            if (!empty($fields_array)) {
              $contants = implode(',', $fields_array);
              $data['constant'] = $contants;
              $params['phraseLabelWeights'] = new PipelineStepParamBinding($data);
              $altered_pre_steps[$key] = $step->setParams($params);
            }
          }
        }

        if (array_key_exists('field', $params)) {
          $field = $params['field'];
          $field_name = $field->getConstant();

          if (array_key_exists('score', $params)) {
            $score = $params['score'];
            $latest_score = floatval($score->getConstant());
            $combined_string_fields = array_diff($combined_string_fields, [$field_name]);
          }
        }
      }

      if ($type == 'QUERY' && $name == 'app') {
        foreach ($combined_string_fields as $field) {
          $new_step = new PipelineStep();
          $new_step->setId('index-text-index-boost');
          $new_step->setTitle("Search field `$field`");
          $params = [];
          $latest_score = $latest_score / 2;

          $field_param = [
            'bind' => '',
            'description' => '',
            'default_value' => NULL,
            'constant' => $field,
          ];

          $params['field'] = new PipelineStepParamBinding($field_param);

          $score_param = [
            'bind' => '',
            'description' => '',
            'default_value' => NULL,
            'constant' => $latest_score . '',
          ];

          $params['score'] = new PipelineStepParamBinding($score_param);

          $text_param = [
            'bind' => 'q',
            'description' => '',
            'default_value' => NULL,
            'constant' => NULL,
          ];

          $params['text'] = new PipelineStepParamBinding($text_param);

          $new_step->setParams($params);
          $altered_pre_steps[] = $new_step;
        }
      }

      // Process and alter post steps.
      $post_steps = $default_pipeline->getPostSteps();
      $altered_post_steps = $post_steps;

      foreach ($post_steps as $key => $step) {
        $params = (array) $step->getParams();

        if (array_key_exists('fields', $params)) {
          $fields = $params['fields'];

          $data = [
            'bind' => $fields->getBind(),
            'description' => $fields->getDescription(),
            'default_value' => $fields->getDefaultValue(),
          ];
          $contants = $fields->getConstant();

          if (isset($contants)) {
            $fields_array = explode(',', $contants);

            if (!empty($index_fields)) {
              $new_fields_combined = array_combine($index_fields, $index_fields);
              $new_fields_combined = str_replace('=', ':', http_build_query($new_fields_combined, '', ','));
              $new_fields_combined = explode(',', $new_fields_combined);
              $fields_array = array_unique(array_merge($fields_array, $new_fields_combined), SORT_REGULAR);
              $contants = implode(',', $fields_array);

              if (!empty($fields_array)) {
                $data['constant'] = $contants;
                $params['fields'] = new PipelineStepParamBinding($data);
                $altered_post_steps[$key] = $step->setParams($params);
              }
            }
          }
        }
      }

      // Set altered steps.
      $default_pipeline->setPreSteps($altered_pre_steps);
      $default_pipeline->setPostSteps($altered_post_steps);

      return $default_pipeline;
    }
    catch (Exception $e) {
      $message = $this->t('Exception when calling PipelinesApi->getDefaultVersion: @message', [
        '@message' => $e->getMessage(),
      ]);

      $this->getLogger()->error($message);
    }
  }

  /**
   * Helper name to get a machine name of a given human name.
   *
   * @param string $human_name
   *   A human name string.
   *
   * @return string
   *   An machine name version of the given human name.
   */
  private function getMachineName($human_name) {
    $human_name .= '_';
    $human_name = mb_strtolower($human_name);
    $human_name = preg_replace('@[^a-z0-9_.]+@', '_', $human_name);
    $human_name = str_replace('__', '_', $human_name);

    return $human_name;
  }

}
