# Search API Search.io

## INTRODUCTION

This module provides a Search.io backend for the
[Search API](https://www.drupal.org/project/search_api) module.

The backend uses the [Search.io](https://www.search.io/) SaaS real-time,
cloud-based search and matching engine. Search.io is a smart, highly-configurable,
real-time search service that enables thousands of businesses worldwide to
provide amazing search experiences on their websites, stores, and applications.

The Search API backend does not support facets or searching. It is useful just
for indexing content into Search.io, improving the recommendations it is able to
make. The backend is capable of indexing more than one site, allowing for
federated recommendations across multiple Drupal sites.

## REQUIREMENTS

1. [Search API](https://www.drupal.org/project/search_api) module.
2. [Search.io SDK for PHP](https://github.com/sajari/sdk-php) library.

## INSTALLATION

The module can be installed via the
[standard Drupal installation process](https://drupal.org/node/1897420).

## CONFIGURATION

See [Search API module's README](https://www.drupal.org/node/2852816) for
instructions.

## MAINTAINERS

This module is maintained by developers at Morpht. For more information on the
company and our offerings, see [morpht.com](https://morpht.com/).
